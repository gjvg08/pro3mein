##<p align="center">Projekt 3 Alignments Musterlösung</p>

###Aufgabe 2
a) Onkogene können bei übermäßiger Aktivierung zur Entstehung von Tumorzellen beitragen. Sie sind auch als Krebs- oder Tumorgene bekannt.

c+d)

Needleman-Wunsch:
&ensp;&ensp;&ensp;Score: 6568	Sequenzidentität: 68.73%

Smith-Waterman:
&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;Score: 6594	Sequenzidentität: 69.07%

e) Ähnlicher Score und Identität, Waterman-Smith skippt anfängliche und endständige Gaps.

<div style="page-break-after: always;"></div>

###Aufgabe 3

a)  
Tra2 reguliert die geschlechtliche Differenzierung des Individuums zum Weibchen hin.  
eyeless reguliert die Entwicklung der Augen bei M. drosophila und kann bei Misexpression zu vermehrten oder fehlenden Augen führen.

b) Zum Beispiel import blosum as bl -> bl.BLOSUM(62)["BASE1" + "BASE2"]
####PTCH2
c) Score: -162

e)  
![PTHC2 vs TRA2, Häufigkeit von Alignmentscores permutierter TRA2-Sequenzen zu PTCH2](random_1000_ptch2_tra2.png)

f) Wahrscheinlichkeit für zufälligen Treffer: 8.0%

<div style="page-break-after: always;"></div>

####eyeless
c) Score: -699

e)  
![eyeless vs TRA2, Häufigkeit von Alignmentscores permutierter TRA2-Sequenzen zu eyeless](random_1000_eyeless_tra2.png)

f) Wahrscheinlichkeit für zufälligen Treffer: 40.4%


###Aufgabe 4

Gapkosten auf 0 erlaubt lange Alignments, da durch Gaps nicht riskiert im Score auf 0 zurück zu fallen.  
Niedrige Mismatchkosten erlauben ebenfalls längere Alignments mit deutlich geringerer Sequenzidentität.  
Mit den richtigen Einstellungen findet sich tatsächlich ein lokales Alignment mit hoher Identität. Bestrafung von Fehlern und nicht zu hohe Belohnung von Matches fördern hier das lokale Alignment.

Alignments:  <br />

Match: 1	Mismatch: -2	Gapkosten: -2  
TTTTAAAG   
TTTTAAAG   
Score: 8	Sequenzidentität: 100.0%  <br />


Match: 1	Mismatch: -2	Gapkosten: -1  
ACGTTTTAAAG   
AC-TTTTAAAG   
Score: 9	Sequenzidentität: 90.91%  <br />


Match: 1	Mismatch: -2	Gapkosten: 0  
C-TTT-----A-AA-AG-C-T---TT-G-C-T-GAAGCAACGGCC------TTGTAAGTCGTAG-AA----AACTATACGTTTTAAAGCT-   
CTTTTAAAGGATAACAGCCATCCGTTGGTCTTAG--GC--C--CCAAAAATTT-T--G--GT-GCAACTCCAA--ATA------AAAG-TA   
Score: 41	Sequenzidentität: 45.05%  <br />

Match: 1	Mismatch: -1	Gapkosten: -2  
TTTTAAAG   
TTTTAAAG   
Score: 8	Sequenzidentität: 100.0%  <br />

Match: 1	Mismatch: -1	Gapkosten: -1  
CTTTGCTGAA-G-CAACGGCC-T-TGTAAGTCGTAG   
CTTT--TAAAGGATAACAGCCATCCGTTGGTCTTAG   
Score: 10	Sequenzidentität: 63.89%  <br />

Match: 1	Mismatch: -1	Gapkosten: 0  
C-TTT-----A-AA-AG-C-T---TT-G-C-T-GAAGCAACGGCC------TTGTAAGTCGTAG-AA----AACTATACGTTTTAAAGCT-   
CTTTTAAAGGATAACAGCCATCCGTTGGTCTTAG--GC--C--CCAAAAATTT-T--G--GT-GCAACTCCAA--ATA------AAAG-TA   
Score: 41	Sequenzidentität: 45.05%  <br />

Match: 1	Mismatch: 0	Gapkosten: -2  
CTTTAAAAGCTTTGCTGAAGCAACGGCCTTGTAAGTCGTAGAAAACTATACGTTTTAAAGCT   
CTTTTAAAGGATAACAG-CCATCCGTTGGTCTTAGGC-CCCAAAAATTTTGGTGCAACTCCA   
Score: 24	Sequenzidentität: 45.16%  <br />

Match: 1	Mismatch: 0	Gapkosten: -1  
CTTTAAAAGCTTTGCTGAAGCAACGGCCTTGTAAGTCGTAGAAAACTATACGTTTTAAAGCT   
CTTTTAAAGGATAACAG-CCATCCGTTGGTCTTAGGC-CCCAAAAATTTTGGTGCAACTCCA   
Score: 26	Sequenzidentität: 45.16%  <br />

Match: 1	Mismatch: 0	Gapkosten: 0  
C-TTT-----A-AA-AG-C-T---TT-G-CTGAAGCAACGGCC-----TTGTAAGTCGTAGAAAACT--ATACGTTTTAAAGCT-   
CTTTTAAAGGATAACAGCCATCCGTTGGTCTTAGGC--C--CCAAAAATT-T---TGGT-G-CAACTCCA-A---ATAAAAG-TA   
Score: 41	Sequenzidentität: 48.24%  <br />

Match: 2	Mismatch: -2	Gapkosten: -2  
CTTTGCTGAA-G-CAACGGCC-T-TGTAAGTCGTAG   
CTTT--TAAAGGATAACAGCCATCCGTTGGTCTTAG   
Score: 20	Sequenzidentität: 63.89%  <br />

Match: 2	Mismatch: -2	Gapkosten: -1  
CTTTAAAAGCTTTGCTGAAGCAACGGCC-T-TGTAAGTCGTA-G----AAAACTATACGTTT--T-AAAGCT   
CTTTTAAAG----GAT-AA-C-A--GCCATCCGTTGGTCTTAGGCCCCAAAA--AT---TTTGGTGCAA-CT   
Score: 41	Sequenzidentität: 55.56%  <br />

Match: 2	Mismatch: -2	Gapkosten: 0  
C-TTT-----A-AA-AG-C-T---TT-G-C-T-GAAGCAACGGCC------TTGTAAGTCGTAG-AA----AACTATACGTTTTAAAGCT-   
CTTTTAAAGGATAACAGCCATCCGTTGGTCTTAG--GC--C--CCAAAAATTT-T--G--GT-GCAACTCCAA--ATA------AAAG-TA   
Score: 82	Sequenzidentität: 45.05%  <br />

Match: 2	Mismatch: -1	Gapkosten: -2  
CTTTGCTGAA-G-CAACGGCC-T-TGTAAGTCGTAGAAAACTATACGTTTTAAAGC   
CTTT--TAAAGGATAACAGCCATCCGTTGGTCTTAG-GCCCCAAAAATTTTGGTGC   
Score: 33	Sequenzidentität: 57.14%  <br />

Match: 2	Mismatch: -1	Gapkosten: -1  
CTTTAAAAGCTTTGCTGAAGCAACGGCC-T-TGTAAGTCGTA-G----AAAACTATACGTTT--T-AAAGCT   
CTTTTAAAG----GAT-AA-C-A--GCCATCCGTTGGTCTTAGGCCCCAAAA--AT---TTTGGTGCAA-CT   
Score: 48	Sequenzidentität: 55.56%  <br />

Match: 2	Mismatch: -1	Gapkosten: 0  
C-TTT-----A-AA-AG-C-T---TT-G-C-T-GAAGCAACGGCC------TTGTAAGTCGTAG-AA----AACTATACGTTTTAAAGCT-   
CTTTTAAAGGATAACAGCCATCCGTTGGTCTTAG--GC--C--CCAAAAATTT-T--G--GT-GCAACTCCAA--ATA------AAAG-TA   
Score: 82	Sequenzidentität: 45.05%  <br />

Match: 2	Mismatch: 0	Gapkosten: -2  
CTTTAAAAGCTTTGCTGAAGCAACGGCCTTGTAAGTCGTAGAAAACTATACGTTTTAAAGCT   
CTTTTAAAGGATAACAG-CCATCCGTTGGTCTTAGGC-CCCAAAAATTTTGGTGCAACTCCA   
Score: 52	Sequenzidentität: 45.16%  <br />

Match: 2	Mismatch: 0	Gapkosten: -1  
CTTTAAAAGCTTTGCTG--AAGCAACGGCCTTGTAAGTCGTAGAAAACTATACGTTTTAAAGCT   
CTTTTAAAGGATAACAGCCATCCGTTGGTC-T-TAGGCCCCA-AAAA-T-TTTGGTGCAACTCC   
Score: 59	Sequenzidentität: 51.56%  <br />

Match: 2	Mismatch: 0	Gapkosten: 0  
C-TTT-----A-AA-AG-C-T---TT-G-CTGAAGCAACGGCC-----TTGTAAGTCGTAGAAAACT--ATACGTTTTAAAGCT-   
CTTTTAAAGGATAACAGCCATCCGTTGGTCTTAGGC--C--CCAAAAATT-T---TGGT-G-CAACTCCA-A---ATAAAAG-TA   
Score: 82	Sequenzidentität: 48.24%

<div style="page-break-after: always;"></div>

###Source Code:

````python
import blosum as bl
import random as rand
from collections import Counter
import matplotlib.pyplot as plt


def create_substitutionmatrix(matchval, mismatch_cost, gap_cost):
    # initialize substitutionmatrix for oncogenes in a similar way as blosum package
    atgc_sub_matrix = dict()
    for el in [c1 + c2 for c1 in "ATGC*" for c2 in "ATGC*"]:
        if '*' in el:
            atgc_sub_matrix[el] = gap_cost  # gapcost
        elif el[0] == el[1]:
            atgc_sub_matrix[el] = matchval  # matchvalue
        else:
            atgc_sub_matrix[el] = mismatch_cost  # mismatchcost
    return atgc_sub_matrix


def create_blosum(num, gap_cost):
    """Creates a blosum matrix from the blosum package substituting the gap_cost with the given
    parameter. """
    blosum_matrix = dict(bl.BLOSUM(num))
    for k, v in blosum_matrix.items():
        if '*' in k:
            blosum_matrix[k] = gap_cost
    return blosum_matrix


def read_fasta(file_path):
    first, second = "", ""
    f = True
    s = False
    with open(file_path) as file:
        file.readline()
        for line in file:
            if line.startswith(">"):
                s = True
                f = False
            elif f:
                first += line.rstrip()
            elif s:
                second += line.rstrip()
    return first, second


def needle(a, b, sub_matrix):
    """Uses a dictionary mem with a key "Score" to point at the integerscore and a key
    "Precursor" to point at another dictionary with the keys "diag, hor, vert" which point to
    booleans, if the taken score was maximal based on any of the directions. """
    mem = dict()
    n = len(a)
    m = len(b)

    # initialize the matrix with gapcosts on leftmost column and uppermost row and the rest of
    # the matrix with 0 and the precursordictionary
    for i in range(n + 1):
        for j in range(m + 1):
            if i == 0 and j > 0:
                mem[(i, j)] = {"Score": mem[(i,j-1)]["Score"] + sub_matrix[b[j - 1] + '*'],
                               "Precursor": {"diag": False, "hor": False, "vert": True}}
            elif j == 0 and i > 0:
                mem[(i, j)] = {"Score": mem[(i-1,j)]["Score"] + sub_matrix[a[i - 1] + '*'],
                               "Precursor": {"diag": False, "hor": True, "vert": False}}
            else:
                mem[(i, j)] = {"Score": 0,
                               "Precursor": {"diag": False, "hor": False, "vert": False}}

    for i in range(1, n + 1):
        for j in range(1, m + 1):
            # pairing score for chars from a and b
            diag = mem[(i - 1, j - 1)]["Score"] + sub_matrix[a[i - 1]+b[j - 1]]

            # pairing score for char from b and gap
            vert = mem[(i - 1, j)]["Score"] + sub_matrix[b[j - 1] + '*']

            # pairing score for char from a and gap
            hor = mem[(i, j - 1)]["Score"] + sub_matrix[a[i - 1] + '*']
            maximum = max(diag, vert, hor)
            if maximum == diag:
                mem[(i, j)]["Score"] = diag
                mem[i, j]["Precursor"]["diag"] = True
            if maximum == vert:
                mem[(i, j)]["Score"] = vert
                mem[i, j]["Precursor"]["vert"] = True
            if maximum == hor:
                mem[(i, j)]["Score"] = hor
                mem[i, j]["Precursor"]["hor"] = True

    return mem


def traceback(mem, a, b, alg):
    """"Expects a matrix filled with alignment scores, the aligned sequences a and b,
    and "needle"/"smith" as to which alignment algorithm was used. Returns an unspecified
    alignment of strings a and b from the pairing matrix mem and its score. If there is more
    than one possible alignment, any will be picked. """
    align_a, align_b = "", ""
    score = 0

    if alg == "needle":
        # start at bottom right
        i = len(a)-1
        j = len(b)-1
        score = mem[(i+1, j+1)]["Score"]

        # this constructs an alignment based on needleman-wunsch
        while i >= 0 and j >= 0:
            if mem[(i+1, j+1)]["Precursor"]["diag"]:
                align_a += a[i]
                align_b += b[j]
                i -= 1
                j -= 1
            elif mem[(i+1, j+1)]["Precursor"]["vert"]:
                align_a += a[i]
                align_b += "-"
                i -= 1
            elif mem[(i+1, j+1)]["Precursor"]["hor"]:
                align_a += "-"
                align_b += b[j]
                j -= 1

        while i >= 0:           # align the rest of a with gaps
            align_a += a[i]
            align_b += "-"
            i -= 1
        while j >= 0:           # align the rest of b with gaps
            align_a += "-"
            align_b += b[j]
            j -= 1

    elif alg == "smith":
        maximum = 0
        max_idx = (0, 0)

        # find maximum and its index; finds first maximum
        for x in range(len(a) + 1):
            for y in range(len(b) + 1):
                if mem[(x, y)]["Score"] >= maximum:
                    maximum = mem[(x, y)]["Score"]
                    max_idx = (x, y)
        i = max_idx[0]
        j = max_idx[1]
        score = maximum

        # traceback until a zero is found
        while True:
            if mem[(i, j)]["Precursor"]["diag"]:
                align_a += a[i - 1]
                align_b += b[j - 1]
                i -= 1
                j -= 1
            elif mem[(i, j)]["Precursor"]["vert"]:
                align_a += a[i - 1]
                align_b += "-"
                i -= 1
            elif mem[(i, j)]["Precursor"]["hor"]:
                align_a += "-"
                align_b += b[j - 1]
                j -= 1
            if mem[(i, j)]["Score"] == 0:
                break

    return align_a[::-1], align_b[::-1], score      # return reverse strings, since chars were
                                                    # added from back to start


def smith(a, b, sub_matrix):
    """Same as needleman-wunsch but modified so leftmost column and uppermost row are
    initialized with zero and maximalization with zero is permitted. """
    mem = dict()
    n = len(a)
    m = len(b)
    for i in range(n + 1):
        for j in range(m + 1):
            mem[(i, j)] = {"Score": 0,           # initialize everything with 0
                           "Precursor": {"diag": False, "hor": False, "vert": False}}

    for i in range(1, n + 1):
        for j in range(1, m + 1):
            # pairing score for chars from a and b
            diag = mem[(i - 1, j - 1)]["Score"] + sub_matrix[a[i - 1] + b[j - 1]]

            # pairing score for char from b and gap
            vert = mem[(i - 1, j)]["Score"] + sub_matrix[b[j - 1] + '*']

            # pairing score for char from a and gap
            hor = mem[(i, j - 1)]["Score"] + sub_matrix[a[i - 1] + '*']
            maximum = max(diag, vert, hor, 0)
            if maximum == 0:
                mem[(i, j)]["Score"] = 0
                continue            # no precursor is added so traceback ends local alignment at 0
            if maximum == diag:
                mem[(i, j)]["Score"] = diag
                mem[i, j]["Precursor"]["diag"] = True
            if maximum == vert:
                mem[(i, j)]["Score"] = vert
                mem[i, j]["Precursor"]["vert"] = True
            if maximum == hor:
                mem[(i, j)]["Score"] = hor
                mem[i, j]["Precursor"]["hor"] = True

    return mem


def identity(align_a, align_b):
    """Takes two alignment strings of same length and returns the ratio of matches / length aka
    the identity of the alignment. """
    length = len(align_a)
    matches = 0
    for i in range(length):
        if align_a[i] == align_b[i] and align_a[i] != '*':
            matches += 1
    return matches / length


def random_align(a, b, count, sub_matrix, out_file):
    """Aligns original sequence of a to a permutation of b count times and appends the scores
    tab-separated to the out_file. Also returns the list of scores. """
    scores = []

    with open(out_file, 'a') as file:
        for i in range(count):
            rand_b = list(b)
            rand.shuffle(rand_b)
            score = needle(a, rand_b, sub_matrix)[(len(a), len(rand_b))]
            scores.append(score["Score"])
            file.write("{}\t".format(score["Score"]))

    return scores


def read_scores(score_file):
    """Takes a path to a file with tab-separated alignment scores in the first line.
        Returns a dictionary with:
            key:    Score
            value:  Occurence of Score"""
    with open(score_file) as file:
        # split along tabs, but remove whitespace, so only floats remain
        line = file.readline().rstrip().split("\t")
        scores = [float(x) for x in line]
    return dict(Counter(scores))


def create_histogram(og_score, scores, gene1, gene2, save=False, out=None):
    lst_scores = [k for k, v in scores.items() for _ in range(v)]
    plt.hist(lst_scores, bins='auto', rwidth=0.8)
    og_line = plt.axvline(x=og_score, color='red', label="Original Score")
    plt.title("Häufigkeit {} zufälliger Alignmentscores von {} und {}".format(sum(scores.values()),
                                                                              gene1,
                                                                              gene2))
    plt.ylabel("Absolute Häufigkeit")
    plt.xlabel("Alignmentscore")
    plt.legend(handles=[og_line])
    if save:
        plt.savefig(out)
    else:
        plt.show()


def random_success_probability(og_score, scores):
    better_scores = 0
    total_scores = 0
    for k, v in scores.items():
        total_scores += v
        if k >= og_score:
            better_scores += v
    return better_scores / total_scores


def main():
    h_kras, m_kras = read_fasta("Oncogenes.fasta")
    eyeless, tra2 = read_fasta("Eyeless_Tra.fasta")
    ptch2, tra2 = read_fasta("PTCH2_TRA2.fasta")
    rna1, rna2 = read_fasta("rRNASeq.fasta")
    atgc_sub_matrix = create_substitutionmatrix(2, 0, -1)
    blosum_matrix = create_blosum(62, -5)

    # a2
    # mem_needle = needle(h_kras, m_kras, atgc_sub_matrix)
    # n_align_a, n_align_b, n_score = traceback(mem_needle, h_kras, m_kras, "needle")
    # n_seq_id = identity(n_align_a, n_align_b)
    #
    # print("Aufgabe2:\n\nNeedleman-Wunsch:")
    # print(n_align_a)
    # print(n_align_b)
    # print("Score: {}\tSequenzidentität: {}%".format(n_score, round(n_seq_id * 100, 2)))
    #
    # mem_smith = smith(h_kras, m_kras, atgc_sub_matrix)
    # s_align_a, s_align_b, s_score = traceback(mem_smith, h_kras, m_kras, "smith")
    # s_seq_id = identity(s_align_a, s_align_b)
    #
    # print("Smith-Waterman:")
    # print(s_align_a)
    # print(s_align_b)
    # print("Score: {}\tSequenzidentität: {}%".format(s_score, round(s_seq_id * 100, 2)))

    # a3 - PTCH2
    mem_needle_ptch2 = needle(ptch2, tra2, blosum_matrix)
    align_a, align_b, score = traceback(mem_needle_ptch2, ptch2, tra2, "needle")
    #
    # print("NW PTCH2 vs TRA2:")
    # print(align_a)
    # print(align_b)
    # print("Score: {}".format(score))
    #
    random_align(ptch2, tra2, 500, blosum_matrix, "random_1000_ptch2_tra2.txt")
    rand_scores = read_scores("random_1000_ptch2_tra2.txt")
    create_histogram(score, rand_scores, "PTCH2", "TRA2")
    rand_success = random_success_probability(score, rand_scores)
    print("Wahrscheinlichkeit für zufälligen Treffer: {}%".format(round(rand_success * 100, 2)))

    # a3 eyeless
    mem_needle_eyeless = needle(eyeless, tra2, blosum_matrix)
    align_a, align_b, score = traceback(mem_needle_eyeless, eyeless, tra2, "needle")
    #
    # print("NW eyeless vs TRA2:")
    # print(align_a)
    # print(align_b)
    # print("Score: {}".format(score))
    #
    random_align(eyeless, tra2, 500, blosum_matrix, "random_1000_eyeless_tra2.txt")
    rand_scores = read_scores("random_1000_eyeless_tra2.txt")
    create_histogram(score, rand_scores, "eyeless", "TRA2")
    rand_success = random_success_probability(score, rand_scores)
    print("Wahrscheinlichkeit für zufälligen Treffer: {}%".format(round(rand_success * 100, 2)))

    # a4
    # for match in range(1, 3):
    #     for mismatch in range(-2, 1):
    #         for gap in range(-2, 1):
    #             sub_matrix = create_substitutionmatrix(match, mismatch, gap)
    #             mem_smith = smith(rna1, rna2, sub_matrix)
    #             align_a, align_b, score = traceback(mem_smith, rna1, rna2, "smith")
    #             seq_id = identity(align_a, align_b)
    #             print("Match: {}\tMismatch: {}\tGapkosten: {}  ".format(match, mismatch, gap))
    #             print(align_a, "  ")
    #             print(align_b, "  ")
    #             print("Score: {}\tSequenzidentität: {}%  \n".format(score,
    #                                                                 round(seq_id * 100, 2)))

    # tests
    # a, b = "AAATGCGGGT", "ATTTGCCCCT"
    # a, b = "AAATGCGGGCATGCTAGCATGCAGCTATCAGCAGCATGCAGCTACACGACACATCATTATATAATATCGCTATATT", "ATTCCCCGCGCGCGCGCGCGCGCCCCGGAATTAGCTGCCCCT"
    # a, b = "AAATGCGGGTA", "ATTTGCT"
    # a, b = "AAAT", "ATTT"
    # test_m_needle = needle(a, b, atgc_sub_matrix)
    # test_align_a, test_align_b, test_score = traceback(test_m_needle, a, b, "needle")
    # test_seq_id = identity(test_align_a, test_align_b)
    # print(test_align_a)
    # print(test_align_b)
    # print("Score: {}\tSequenzidentität: {}%".format(test_score, round(test_seq_id * 100, 2)))
    # # print({k: v for k, v in sorted(atgc_sub_matrix.items(),
    # #                                key=lambda item: item[1], reverse=True)})
    # print([(k,test_m_needle[k]) if k[0] == 0 else None for k in test_m_needle])
    # score2 = 0
    # for c1,c2 in zip(test_align_a,test_align_b):
    #     if c1 == c2:
    #         score2 += 2
    #     elif c1 == "-" or c2 == "-":
    #         score2-=1
    # print(score2)

    # test_m_smith = smith(a, b, atgc_sub_matrix)
    # test_align_a, test_align_b, test_score = traceback(test_m_smith, a, b, "smith")
    # test_seq_id = identity(test_align_a, test_align_b)
    # print(test_align_a)
    # print(test_align_b)
    # print("Score: {}\tSequenzidentität: {}%".format(test_score, round(test_seq_id * 100, 2)))
    # print({k: v for k, v in sorted(atgc_sub_matrix.items(),
    #                                key=lambda item: item[1], reverse=True)})
    # print(test_m_smith)


main()

````
